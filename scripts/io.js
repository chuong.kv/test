module.exports = async (io) => {
    const messages = []
    io.on("connection", async (socket) => {
        socket.emit("messages", messages)
        socket.on("send", async (data) => {
            messages.push(data)
            io.emit("message", data)
        })
    })
}