const express = require('express');
const helmet = require('helmet');
const xss = require('xss-clean');
const compression = require('compression');
const cors = require('cors');
const httpStatus = require('http-status');
const path = require('path')

const config = require('./configs/config');
const morgan = require('./configs/morgan');
const errors = require('./middlewares/error');
const ApiError = require('./utils/ApiError');

const application = express();

if (config.env !== 'test') {
  application.use(morgan.successHandler);
  application.use(morgan.errorHandler);
}

// set security HTTP headers
application.use(helmet());

// parse json request body
application.use(express.json());

// parse urlencoded request body
application.use(express.urlencoded({ extended: true }));

// sanitize request data
application.use(xss());

// gzip compression
application.use(compression());

// enable cors
application.use(cors());
application.options('*', cors());

application.use('/', express.static(path.join(__dirname, '..', 'public')))

// send back a 404 error for any unknown api request
application.use((req, res, next) => {
  next(new ApiError(httpStatus.NOT_FOUND, 'Not found'));
});

// convert error to ApiError, if needed
application.use(errors.errorConverter);

// handle error
application.use(errors.errorHandler);

module.exports = application;
