const http = require("http")
const config = require('./configs/config');
const logger = require('./configs/logger');
const application = require("./application")
const runSocket = require("./io")
const socketio = require("socket.io")

let server;
(async () => {
    const _server = http.createServer(application)
    const io = socketio(_server, {
        cors: {
            origin: "*"
        }
    })
    server = _server.listen(config.port, () => {
      runSocket(io)   
      logger.info(`Listening to port ${config.port}`);
    })
})();

const exitHandler = () => {
  if (server) {
    server.close(() => {
      logger.info('Server closed');
      process.exit(1);
    });
  } else {
    process.exit(1);
  }
};

const unexpectedErrorHandler = (error) => {
  logger.error(error);
  exitHandler();
};

process.on('uncaughtException', unexpectedErrorHandler);
process.on('unhandledRejection', unexpectedErrorHandler);

process.on('SIGTERM', () => {
  logger.info('SIGTERM received');
  if (server) {
    server.close();
  }
});
